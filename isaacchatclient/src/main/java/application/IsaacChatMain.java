package application;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Scanner;

import com.vdurmont.emoji.EmojiParser;

import isaacchatclient.IsaacChatClient;
import model.User;

public class IsaacChatMain {
	public static void main(String[] args) throws UnknownHostException, IOException {
		
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		
		System.out.print(EmojiParser.parseToUnicode("\n" + 
				"Hello, Welcome to Isaac Chat, a simple project designed to "
				+ "demonstrate Socket communication in the field of computer networks :smile:\n"
				+ " Let's start! Enter your name: "));
		
		String userName = scanner.nextLine();
		User user = new User(userName);
		
		System.out.print("Type the server address: ");
		String serverAddress = scanner.nextLine();
		
		IsaacChatClient isaacChatClient = new IsaacChatClient(user, serverAddress, 8083);
		new Thread(isaacChatClient).start();
		
		while(true) {
			System.out.print(":");
			isaacChatClient.sendMessageToAllUsers(scanner.nextLine());
		}
	}
}