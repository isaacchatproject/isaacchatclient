package isaacchatclient.exception;

public class ConnectionException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ConnectionException() {
	}

	public ConnectionException(String arg0) {
		super(arg0);
	}

	public ConnectionException(String message, Throwable exception) {
		super(message, exception);
	}
	
}
