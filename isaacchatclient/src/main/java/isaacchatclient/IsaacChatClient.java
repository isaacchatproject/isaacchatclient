package isaacchatclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import com.vdurmont.emoji.EmojiParser;

import isaacchatclient.exception.ConnectionException;
import model.User;

/**
 * A Client to use the IsaacChatProject
 * 
 * @author Felipe
 *
 */
public class IsaacChatClient implements Runnable {

	private static Socket socket;
	private static User user;
	private static PrintWriter printWriter;
	private static BufferedReader bufferedReader;

	public IsaacChatClient(User user, String serverAddress, Integer serverPort) {
		IsaacChatClient.user = user;
		createSocketConnection(serverAddress, serverPort);
	}

	private void createSocketConnection(String serverAddress, Integer serverPort) {

		try {

			IsaacChatClient.socket = new Socket(serverAddress, serverPort);
			IsaacChatClient.printWriter = new PrintWriter(socket.getOutputStream(), true);
			IsaacChatClient.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		} catch (IOException error) {

			throw new ConnectionException(
					String.format("The server is not available, your connection was denied: %s", error.getMessage()));

		}

	}

	/**
	 * A method to send message to server socket
	 * 
	 * @param message
	 */
	public void sendMessageToAllUsers(String message) {
		printWriter.println(EmojiParser.parseToUnicode(String.format("%s Says: %s", user.getNickname(), message)));
	}

	@Override
	public void run() {
		while (!Thread.interrupted()) {
			try {
				if (bufferedReader.ready())
					System.out.println(bufferedReader.readLine());
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

}
